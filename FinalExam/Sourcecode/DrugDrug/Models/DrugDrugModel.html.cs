using System; 
using System.ComponentModel.DataAnnotations; 
using Microsoft.AspNetCore.Identity; 


namespace DrugDrug.Models {     
    public class DrugUser :  IdentityUser{         
        public string  FirstName { get; set;}         
        public string  LastName { get; set;}     
    } 
    public class Drug {                  
        public string DrugName { get; set; }         
        public string DrugDetail { get; set; }         
        public int stock  { get; set; }         
 
        public int DrugId {get; set;} 
        public string DrugUserId  {get; set;}      
        public DrugUser  postUser {get; set;} 
    }     
} 