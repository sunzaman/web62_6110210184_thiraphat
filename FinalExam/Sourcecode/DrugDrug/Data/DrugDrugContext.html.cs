using DrugDrug.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore; 

namespace DrugDrug.Data{
    public class DrugDrugContext : IdentityDbContext<DrugUser> {
        public DbSet<Drug> Drug { get; set; }
    
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=NewsReport.db");			
        }
    }
    
} 