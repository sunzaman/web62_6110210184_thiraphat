using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DrugDrug.Data;
using DrugDrug.Models;

namespace DrugDrug.Pages.DrugAdmin
{
    public class CreateModel : PageModel
    {
        private readonly DrugDrug.Data.DrugDrugContext _context;

        public CreateModel(DrugDrug.Data.DrugDrugContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["DrugUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Drug Drug { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Drug.Add(Drug);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}