using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DrugDrug.Data;
using DrugDrug.Models;

namespace DrugDrug.Pages.DrugAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly DrugDrug.Data.DrugDrugContext _context;

        public DetailsModel(DrugDrug.Data.DrugDrugContext context)
        {
            _context = context;
        }

        public Drug Drug { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Drug = await _context.Drug
                .Include(d => d.postUser).FirstOrDefaultAsync(m => m.DrugId == id);

            if (Drug == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
