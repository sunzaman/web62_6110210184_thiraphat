using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DrugDrug.Data;
using DrugDrug.Models;

namespace DrugDrug.Pages.DrugAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly DrugDrug.Data.DrugDrugContext _context;

        public DeleteModel(DrugDrug.Data.DrugDrugContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Drug Drug { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Drug = await _context.Drug
                .Include(d => d.postUser).FirstOrDefaultAsync(m => m.DrugId == id);

            if (Drug == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Drug = await _context.Drug.FindAsync(id);

            if (Drug != null)
            {
                _context.Drug.Remove(Drug);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
