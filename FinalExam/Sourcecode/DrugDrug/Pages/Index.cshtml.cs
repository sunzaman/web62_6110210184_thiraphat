﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DrugDrug.Data;
using DrugDrug.Models;

namespace DrugDrug.Pages
{
    public class IndexModel : PageModel
    {
        private readonly DrugDrug.Data.DrugDrugContext _context;
        public IndexModel(DrugDrug.Data.DrugDrugContext context)
        {  
            _context = context;
        }

        public IList<Drug> Drug { get;set; }
    public async Task OnGetAsync()
        {
            Drug = await _context.Drug
            .ToListAsync();
        } 
    }
}
