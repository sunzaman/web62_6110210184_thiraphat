using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NewsReport.Data;
using NewsReport.Models;

namespace NewsReport.Pages.NewsAdmin
{
    public class IndexModel : PageModel
    {
        private readonly NewsReport.Data.NewsReportContext _context;

        public IndexModel(NewsReport.Data.NewsReportContext context)
        {
            _context = context;
        }

        public IList<News> News { get;set; }
        public async Task OnGetAsync()
        {
            News = await _context.newsList
                .Include(n => n.NewsCat).ToListAsync();
        }
    }
}
